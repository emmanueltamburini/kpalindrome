 #!/bin/bash

 echo run app Script
 echo delete open process
	kill -9 `ps -ef|grep -v grep |grep "java -jar workspace/backend/target/palindrome-0.0.1-SNAPSHOT.jar"| awk '{print $2}'`
	kill -9 `ps -ef|grep -v grep |grep "serve -s workspace/frontend/build"| awk '{print $2}'`
 echo building servers
	 cd workspace/frontend/
	 npm run build
	 cd ../backend/
	 mvn clean package
	 cd ../../

 echo running servers
	java -jar workspace/backend/target/palindrome-0.0.1-SNAPSHOT.jar &
 	serve -s workspace/frontend/build &
	wait
