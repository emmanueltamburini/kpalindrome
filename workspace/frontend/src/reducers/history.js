import {
  PUSH_HISTORY,
} from '../types';

const initialState = {
  history: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PUSH_HISTORY:
      return {
        history: [...state.history, action.history],
      };

    default: return state;
  }
};
