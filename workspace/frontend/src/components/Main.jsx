import React, { Component } from 'react';
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import Form from './Form';
import History from './History';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/Main.css';


class Main extends Component{
  render() {
    return (
      <Container fluid className="Main">
        <Row className="justify-content-md-center">
          <Col md={{ offset: 5 }}>
            <h3>Kpalindrome</h3>
          </Col>
        </Row>
        <Row>
          <Col>
            <Tabs id="controlled-tab-example">
              <Tab eventKey="home" title="Home">
                <Form />
              </Tab>
              <Tab eventKey="profile" title="Profile">
                <History />
              </Tab>
            </Tabs>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Main;