import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'

import { pushHistory } from '../actions/History';
import { postPalindrome } from '../requests/History';

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      word: '',
      kvalue: 0,
      show: false,
      answer: false,
    };
    this.onChange = this.onChange.bind(this);
    this.sendWord = this.sendWord.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  sendWord(e) {
     const {
      word,
			kvalue,
    } = this.state;

     const {
      dispatch,
    } = this.props;

    const body = {
    	word,
			kvalue: parseInt(kvalue, 10),
    }

		postPalindrome(body)
		.then((response) => {
			if(response && response.status === 200) {
				this.setState({
					show: true,
					answer: response.data.answer,
				});
				body.answer = response.data.answer;
				dispatch(pushHistory(body));
			}
		});
  }

	handleClose(e) {
	    this.setState({
	    	show: false,
	      word: '',
	      kvalue: 0,
	    })
	}

  render() {
     const {
      word,
			kvalue,
			show,
			answer,
    } = this.state;

    return (
      <div>
	      <Modal
	      	show={show}
	      	onHide={this.handleClose}
  	      size="sm"
		      aria-labelledby="contained-modal-title-vcenter"
		      centered
      	>
	        <Modal.Header closeButton>
	          <Modal.Title>Test result</Modal.Title>
	        </Modal.Header>
	        <Modal.Body>{`${word} is ${answer ? '' : 'not '}a Kpalindrome with k = ${kvalue}`}</Modal.Body>
	        <Modal.Footer>
	          <Button variant="primary" onClick={this.handleClose}>
	            Close
	          </Button>
	        </Modal.Footer>
	      </Modal>
        <div className="row m-2">
          <div className="col-12 col-md-6 p-2">
            <label htmlFor="word">
              {'Word '}
              <input
                name="word"
                type="text"
                value={word}
                onChange={this.onChange}
              />
            </label>
          </div>
          <div className="col-12 col-md-6 p-2">
            <label htmlFor="kvalue">
              {'K value '}
              <input
                name="kvalue"
                type="number"
                value={kvalue}
                onChange={this.onChange}
              />
            </label>
          </div>
          <div className="w-50">
			      <Button variant="primary" onClick={this.sendWord}>
			        Send
			      </Button>
          </div>
        </div>
      </div>
    )
  }
}

Form.defaultProps = {
};

Form.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    history: state.history,
  };
}

export default connect(mapStateToProps)(Form);