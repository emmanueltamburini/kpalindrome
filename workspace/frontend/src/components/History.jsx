import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from 'react-bootstrap/Table'

import 'bootstrap/dist/css/bootstrap.min.css';

class History extends Component {
  render() {
     const {
      history,
    } = this.props;

    return (
      <div>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>Word</th>
              <th>k value</th>
              <th>Answer</th>
            </tr>
          </thead>
          <tbody>
          {
            history.map((element, index) => (
              <tr
                key={`${index}-${element.word}`}
              >
                <td>{element.word}</td>
                <td>{element.kvalue}</td>
                <td>{element.answer ? 'True' : 'False'}</td>
              </tr>
            ))
          }
          </tbody>
        </Table>
      </div>
    )
  }
}

History.defaultProps = {
  history: [],
};

History.propTypes = {
  history: PropTypes.arrayOf(PropTypes.shape({
    word: PropTypes.string.isRequired,
    kvalue: PropTypes.number.isRequired,
    answer: PropTypes.bool.isRequired,
  })),
};

function mapStateToProps(state) {
  return {
    history: state.history.history,
  };
}

export default connect(mapStateToProps)(History);