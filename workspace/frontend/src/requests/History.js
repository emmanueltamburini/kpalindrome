import axios from './axios';

export function postPalindrome(body) {
  return axios().post('/palindrome', body)
    .then((response) => response)
    .catch((err) => err.response);
}