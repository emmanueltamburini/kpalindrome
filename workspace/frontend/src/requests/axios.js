import axios from 'axios';

export default () => {
  const ip = process.env.REACT_APP_BACKEND_IP || 'localhost';
  const port = process.env.REACT_APP_BACKEND_PORT || 8080;
  const route = `http://${ip}:${port}`;

  return axios.create({
    baseURL: route,
  });
};
