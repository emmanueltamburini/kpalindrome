import {
  PUSH_HISTORY,
} from '../types';

export function pushHistory(history) {
  return {
    type: PUSH_HISTORY,
    history,
  };
}
