import React from 'react';
import Main from './components/Main';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <Main />
  );
}

export default App;
