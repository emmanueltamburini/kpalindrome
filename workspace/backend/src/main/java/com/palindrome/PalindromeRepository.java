package com.palindrome;


import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.palindrome.Palindrome;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
public interface PalindromeRepository extends CrudRepository<Palindrome, Integer> {
    List<Palindrome> findByWordAndKvalue(String word, Integer kvalue);
}