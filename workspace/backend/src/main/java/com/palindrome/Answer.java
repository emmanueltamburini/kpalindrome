package com.palindrome;

public class Answer {
	private boolean answer;
	private boolean cached;

	public Answer(boolean answer, boolean cached) {
		super();
		this.answer = answer;
		this.cached = cached;
	}

	public boolean getAnswer() {
		return answer;
	}

	public boolean getCached() {
		return cached;
	}
}
