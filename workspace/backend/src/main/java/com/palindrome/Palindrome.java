package com.palindrome;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class Palindrome implements Kpalindrome {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;

  private String word;

  private Integer kvalue;

	public boolean isPalindrome(String word) {
		int base = 0;
		int top = word.length() - 1;

		while (base < top) {
			if (word.charAt(base) == word.charAt(top)) {
				base++;
				top--;
			} else {
				return false;
			}
		}
		return true;
	}

	public boolean isKpalindrome(String word, int k) {
		if(word.length() <= 1) {
			return true;
		}
		
		if (k == 0) {
			return isPalindrome(word);
		}
		
		String aux = word;
		
		for (; k != 0;) {
			if (aux.charAt(0) == aux.charAt(aux.length() - 1)) {
				aux = aux.substring(1, aux.length() - 1);
			} else {
				aux = aux.substring(0, aux.length() - 1);
				k--;
			}

			if(isPalindrome(aux)) {
				return true;
			}
		}
		return isPalindrome(aux);
	}
  
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getWord() {
    return word;
  }

  public void setWord(String word) {
    this.word = word;
  }

  public Integer getKvalue() {
    return kvalue;
  }

  public void setKvalue(Integer kvalue) {
    this.kvalue = kvalue;
  }


}