package com.palindrome;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:5000", "http://157.245.168.201:5000" })
@Controller
@RequestMapping
public class MainController {
  @Autowired 
  private PalindromeRepository palindromeRepository;

  @PostMapping(path="/palindrome")
  public @ResponseBody Answer isKpalindrome (@RequestBody Palindrome palindrome) {
	  List<Palindrome> result = palindromeRepository.findByWordAndKvalue(palindrome.getWord(), palindrome.getKvalue());
	  boolean cached = result.isEmpty();
	  palindromeRepository.save(palindrome);
	  return new Answer(palindrome.isKpalindrome(palindrome.getWord(), palindrome.getKvalue()), cached);
  }
  
  @GetMapping(path="/all")
  public @ResponseBody Iterable<Palindrome> getAllUsers() {
        return palindromeRepository.findAll();
  }
}